---
author: à compléter
title: Méthodes de chiffrement
---

# Méthodes de chiffrement

![](../images/chiffrement.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Présentation générale"

    Une **méthode de chiffrement** est une méthode permettant de rendre illisible un texte afin de protéger une information. Dans cette partie, on présente quelques-unes de ces méthodes parmi celles qui sont les plus classiques en cryptographie :

     - les chiffrements par encodage ;
     - les chiffrements symétriques ;
     - les chiffrements asymétriques ;
     - les fonctions de hachages.

    Ces différentes méthodes peuvent également être utiles pour résoudre les challenges proposés à la section 
    <a href='../../3.Challenges/presentation' target='_blank'>Challenges</a> ou sur les plateformes présentées à 
    la section <a href='../../4.Sites_challenges/presentation' target='_blank'>Plateformes de challenges</a>.



## Chiffrements par encodage
En informatique, chaque caractère est associé, via une table d'encodage spécifique telle que l'
<a href="https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange" target="_blank">ASCII</a>, l'<a href="https://fr.wikipedia.org/wiki/ISO/CEI_8859-1" target="_blank">ANSI</a> ou l'<a href="https://fr.wikipedia.org/wiki/Unicode" target="_blank">Unicode</a>, à une valeur numérique qui est traduite ensuite en écriture binaire, c'est-à-dire avec des 0 et des 1. Les **chiffrements par encodage** sont des méthodes de chiffrement très simples basées sur cette représentation des caractères.

- Chiffrement en décimal : ce chiffrement est le plus simple car on remplace directement chaque caractère par sa valeur numérique. Par exemple, `Cybersécurité` s'écrit 
`67 121 98 101 114 115 233 99 117 114 105 116 233`
- Chiffrement en hexadécimal : dans ce chiffrement, la valeur numérique associée à chaque caractère est traduite, non pas en binaire, mais en hexadécimal (c'est-à-dire en base 16) en utilisant 16 symboles : 
0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E et F. Par exemple, `Cybersécurité` s'écrit `43 79 62 65 72 73 e9 63 75 72 69 74 e9`
- <a href='https://fr.wikipedia.org/wiki/Base64' target='_blank'>Chiffrement en Base 64</a> : le principe de ce chiffrement, basé sur 64 caractères choisis pour être disponibles sur la majorité des systèmes, 
consiste, dans l'écriture binaire de la chaîne de caractères à chiffrer, à regrouper les bits par groupes de 24 bits afin de pouvoir les remplacer par un groupe de quatre caractères prédéterminés. Lorsque ces 
regroupement par groupes de 24 bits ne peuvent être réalisés car il n'y a pas assez de bits, on ajoute une ou plusieurs fois le caractère spécial `=` aux 64 caractères initiaux. Par exemple, 
`Cybersécurité` s'écrit `Q3liZXJz6WN1cml06Q==`. Il existe des variantes de ce chiffrement comme le chiffrement en Base 45 utilisant 45 caractères, et le chiffrement en Base 32 utilisant 32 caractères.

## Chiffrements symétriques
La <a href='https://fr.wikipedia.org/wiki/Cryptographie_sym%C3%A9trique' target='_blank'>cryptographie symétrique</a>, également dite _à clé secrète_, est la plus ancienne forme de chiffrement. 
Elle permet à la fois de chiffrer et de déchiffrer des messages à l'aide d'une **même clé**. On a des traces de son utilisation par les Egyptiens vers 2000 av. J.-C.

- <a href='https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage' target='_blank'>Chiffrement de César</a> : le texte chiffré s'obtient en remplaçant chaque lettre du texte clair original par une 
lettre à distance fixe, toujours du même côté, dans l'ordre de l'alphabet (pour les dernières lettres, dans le cas d'un décalage à droite, on reprend l'alphabet au début). Par exemple, le mot 
`CYBERSECURITE` avec la clé `H` (c'est-à-dire que l'on décale `A` sur `H`, `B` sur `I`, etc.) est chiffré par `JFILYZLJBYPAL`.
- <a href='https://fr.wikipedia.org/wiki/ROT13' target='_blank'>ROT13</a> : ce chiffrement est un cas particulier du chiffrement de César où l'on effectue un décalage de treize caractères. Par 
exemple, le mot `CYBERSECURITE` est chiffré par `PLOREFRPHEVGR`.
- <a href='https://fr.wikipedia.org/wiki/Chiffre_de_Vigen%C3%A8re' target='_blank'>Chiffrement de Vigenère</a> : le texte chiffré s'obtient en découpant le texte initial en groupes de lettres de 
même longueur que le mot-clé choisi, puis en appliquant successivement sur chacun des caractères de ces groupes un chiffrement de César dont le décalage est fourni par le caractère correspond dans le 
mot-clé. Par exemple, le mot `CYBERSECURITE` avec le mot-clé `NSI` est chiffré par `PQJRJARUCEABR`.


## Chiffrements asymétriques
La <a href='https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique' target='_blank'>cryptographie asymétrique</a>, également dite _à clé publique_, est un domaine relativement récent de la 
cryptographie. Elle permet d'assurer la confidentialité d'une communication, ou d'authentifier les participants, sans que cela repose sur une donnée secrète partagée entre ceux-ci, contrairement à la 
cryptographie symétrique qui nécessite ce secret partagé préalable. L'un des chiffrements asymétriques les plus connus est le <a href='https://fr.wikipedia.org/wiki/Chiffrement_RSA' target='_blank'>
chiffrement RSA</a>. 

## Fonctions de hachage
Une <a href='https://fr.wikipedia.org/wiki/Fonction_de_hachage' target='_blank'>fonction de hachage</a> est un algorithme qui prend en entrée des données de n'importe quelle taille et les transforme en une valeur de taille fixe, appelée **empreinte**, 
**hash** ou **hachage**. Cette transformation est unidirectionnelle, ce qui signifie qu'il est difficile (voire impossible) de revenir aux données originales à partir du hash (ce qui est très utile en 
informatique et en cryptographie). Ce type de fonction est généralement utilisé pour vérifier des mots de passe sans avoir à 
les connaître réellement, pour vérifier qu'un téléchargement de fichiers s'est déroulé correctement et est non corrompu, 
pour vérifier la signature électronique d'un personne, etc.

Pour une fonction de hachage donnée, l'empreinte obtenue est **toujours de la même taille**, quelle que soit la taille de la donnée d'entrée, et est généralement renvoyée sous forme de caractères hexadécimaux. De plus, une petite modification des 
données d'entrée fournit automatiquement une empreinte différente.

En principe, la seule façon de retrouver la donnée initiale à partir d'un hash est d'utiliser la méthode par _brute force_. Toutefois, il a été prouvé que certaines fonctions de hachage, comme MD5 et SHA-1, pouvaient être 
vulnérables à certains types d'attaques et sont donc maintenant déconseillées.

- <a href='https://fr.wikipedia.org/wiki/MD5' target='_blank'>MD5</a> : MD5 est utilisé pour la vérification d'intégrité des fichiers, la création de signatures numériques, et dans certains protocoles de sécurité. Cependant, son 
utilisation est désormais déconseillée pour la sécurité des données sensibles comme les mots de passe. Son empreinte est de 32 caractères hexadécimaux.
- <a href='https://fr.wikipedia.org/wiki/SHA-1' target='_blank'>SHA-1</a> : SHA-1 est une fonction de hachage datant de 1995, mais est maintenant déconseillée pour la sécurité des données sensibles. 
Elle produit une empreinte de 40 caractères hexadécimaux.
- <a href='https://fr.wikipedia.org/wiki/SHA-2' target='_blank'>SHA-256</a> : SHA-256 est une norme de hachage, issu du SHA-2 Secure Hash Algorithm, qui produit une empreinte de 64 caractères hexadécimaux. 
Il le nouveau standard recommandé en matière de hachage cryptographique.
- <a href='https://fr.wikipedia.org/wiki/SHA-2' target='_blank'>SHA-512</a> : SHA-512 est une norme de hachage, issu du SHA-2 Secure Hash Algorithm, qui produit une empreinte de 128 caractères hexadécimaux.