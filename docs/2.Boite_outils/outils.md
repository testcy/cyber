---
author: à compléter
title: Outils
---

# Outils

![](../images_boite/boite2-1.jpg){: .center }


!!! tip "femme"

    à compléter

Il existe une variété d'outils, dont nous ne mentionnons ici qu'un échantillon, qui peuvent se révéler extrêmement utiles non seulement pour la résolution de challenges, mais également dans la vie privée et publique.

Dans le cadre des challenges, ces outils offrent des solutions pratiques et efficaces pour surmonter les obstacles, organiser les idées, et atteindre les objectifs fixés.

En dehors de ces contextes spécifiques, ils jouent également un rôle important dans la vie privée et publique, par exemple dans le cadre 
de l'**hygiène numérique** qui consiste à vérifier qu'il n'y a pas sur le Web des éléments non désirés à notre sujet, ou encore dans la vérification des sources d'informations pour lutter contre les _fake news_.


## Hygiène numérique

 - <a href='https://epieos.com/' target='_blank'>Epieos</a> : outil permettant de rechercher des comptes compromis à partir 
    d'une adresse mail ou d'un numéro de téléphone
 - <a href='https://db-leaks.com/' target='_blank'>DBLeaks</a> : outil permettant de rechercher des comptes compromis à partir d'une adresse mail ou d'un numéro de téléphone
 - <a href='https://pimeyes.com/en' target='_blank'>Pimeyes</a> : outil permettant de rechercher des photos d'une personne sur 
    le Web, hors réseaux sociaux, à partir d'une photo donnée de cette personne


## Bases de données

 - <a href='http://inatheque.ina.fr/' target='_blank'>Bibliothèque INA</a> : archives TV, vidéo, radio
 - <a href='https://gallica.bnf.fr/accueil/fr/content/accueil-fr?mode=desktop' target='_blank'>Gallica</a> : base de données de la BNF
 - <a href='https://archive.org/details/books' target='_blank'>Internet Archive Books</a> : recherche de livres sur le web
 - <a href='https://www.data.gouv.fr/fr/datasets/liste-publique-des-organismes-de-formation-l-6351-7-1-du-code-du-travail/' target='_blank'>Organismes de formation</a> : liste des organismes de formation en France
 - <a href='https://annuaire-entreprises.data.gouv.fr/' target='_blank'>Annuaire d'entrepises</a> : synthèse des informations sur les entreprises françaises (SIREN, SIRET, etc.)
 - <a href='https://opencorporates.com/' target='_blank'>OpenCorporates</a> : recherche de sociétés dans le monde
 - <a href='https://www.infogreffe.fr/' target='_blank'>Registre de commerce</a> : greffes des tribunaux de commerce
 


## Chiffrement et déchiffrement de données

 - <a href='https://gchq.github.io/CyberChef/' target='_blank'>CyberChef</a> : outil développé par les services 
    secrets du Royaume-Unis, il sert à manipuler et transformer des données
 - <a href='https://www.dcode.fr/' target='_blank'>Dcode</a> : un grand classique pour le chiffrement et le déchiffrement 
    de données
 - <a href='https://md5decrypt.net/' target='_blank'>Md5decrypt</a> : outil pour décrypter des hashs


## Géolocalisation 
 
  - <a href='https://www.geoportail.gouv.fr/' target='_blank'>Géoportail</a> : outil permettant la recherche et l'affichage de données géographiques
  - <a href='https://www.google.fr/maps/preview' target='_blank'>Google map</a> : outil permettant la recherche et l'affichage sur des cartes géographiques
  - <a href='https://remonterletemps.ign.fr/' target='_blank'>Remonter le temps</a> : outil permettant la comparaison de cartes et de photos aériennes à des dates différentes 
	

## Manipulation d'images

 - <a href='https://www.google.fr/' target='_blank'>Google Lens</a> : outil permettant de faire de la recherche 
    inversée d'images
 - <a href='https://www.gimp.org/downloads/' target='_blank'>GIMP</a> : un grand classique pour le traitement d'images
 - <a href='https://www.aperisolve.com/' target='_blank'>Aperi'Solve</a> : outil permettant l'exploration d'images 
    (métadonnées, filtres, superposition, etc.)
 - <a href='https://www.colorpilot.com/exif.html' target='_blank'>Exif Pilot</a> : outil permettant d'extraire les données Exif d'une image
 - <a href='https://www.img2go.com/fr/comparez-des-images' target='_blank'>img2go</a> : outil permettant la 
    comparaison de deux images
	

## Réseaux

 - <a href='https://www.wireshark.org/download.html' target='_blank'>Wireshark</a> : outil permettant le décodage de trames réseaux
 - <a href='https://curl.se/' target='_blank'>Curl</a> : outil permettant de récupérer des données disponibles sur un réseau


## Sciences

 - <a href='https://www.ncbi.nlm.nih.gov/genbank/' target='_blank'>GenBank</a> : banque de séquences nucléiques (ADN, ARN, génomes, etc.)
 - <a href='https://projecteuclid.org/' target='_blank'>Project Euclid</a> : banque d'articles de recherche en Mathématiques et Statistiques


## Transports

  - <a href='http://www.worldlicenseplates.com/' target='_blank'>Plaques d'immatriculation</a> : outil permettant de vérifier et d'nalyser les plaques d'immatriculation de tous les pays du monde
  - <a href='https://www.flightera.net/' target='_blank'>Trafic aérien</a> : outil permettant de consulter des horaires de vols (anciens et actuels)
  - <a href='https://www.marinetraffic.com/en/ais/home/centerx:-12.0/centery:25.0/zoom:4' target='_blank'>Trafic maritime</a> : outil permettant de consulter le trafic maritime

## Web

 - <a href='https://portswigger.net/burp/communitydownload' target='_blank'>Burp Suite</a> : outil permettant 
    l'interception, l'analyse, la modification et le renvoie de requêtes Web
 - <a href='https://beeceptor.com/' target='_blank'>beeceptor</a> : outil permettant la création de serveurs 
    fictifs pour détourner des requêtes Web
 - <a href='https://web.archive.org/' target='_blank'>WayBack Machine</a> : outil permettant la récupération 
    d'images antérieures de sites Web
 - <a href='https://whatsmyname.app/' target='_blank'>WhatsMyName</a> : outil permettant d'identifier différents comptes à partir d'un pseudo
 - <a href='https://whois.domaintools.com/', target='_blank'>WHOIS Domain</a> : outil permettant d'obtenir différentes informations à partir d'un nom de domaine