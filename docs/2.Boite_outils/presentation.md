---
author: à compléter
title: Introduction
---

!!! abstract "Présentation générale"

    Il existe de nombreuses applications et logiciels **gratuits** permettant de coder ou décoder des données, de 
    manipuler des images, de rechercher des adresses mails compromises, d'intercepter un trafic réseau et d'en décoder 
    les trames, d'intercepter et de manipuler des requêtes HTTP sur le Web, etc.

    Dans cette section, on présente succinctement quelques-uns de ces outils, qui seront très 
    utiles pour la résolution des challenges proposés à la section <a href='../../3.Challenges/presentation'>Challenges</a> ou sur les plateformes 
    présentées à la section <a href='../../4.Sites_challenges/presentation'>Plateformes de challenges</a>.

    On propose également des **fiches ressources** pour aider ou approfondir quelques notions, comme le chiffrement des données.

