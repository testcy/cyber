---
author: à compléter
title: Liste des challenges
---

En cliquant sur les entêtes, vous pouvez trier selon la catégorie, le titre du challenge, le nombre de flags ou la difficulté.

<center>
    <table>
        <thead>
            <tr>
                <th>Catégorie &#8645;</th>
                <th>Titre &#8645;</th>
                <th>Nombre de flags &#8645;</th>
				<th>Difficulté &#8645;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Algorithmique</td>
                <td><a href="../ressources_challenges_algo/BlackbeardsHiddenTreasures/BlackbeardsHiddenTreasures">Blackbeard’s Hidden Treasures</a></td>
                <td>12</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>Algorithmique</td>
                <td><a href="../ressources_challenges_algo/Cadenas/coffre_cadenas">Cadenas</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>Algorithmique</td>
                <td><a href="../ressources_challenges_algo/CadenasAlea/cadenas">Cadenas dynamiques</a></td>
                <td>3</td>
				<td>⭐</td>
            </tr>
            <tr>
                <td>Cryptographie</td>
                <td><a href="../ressources_challenges_crypto/CryptageRenforcé/cryptage_renforce">Cryptage renforcé</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
            <tr>
                <td>Cryptographie</td>
                <td><a href="../ressources_challenges_crypto/DossierProtégé/dossier_protege">Dossier protégé</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Cryptographie</td>
                <td><a href="https://www.challenges-kids.fr/categories/crypto/chall7/">Grille tournante</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="../ressources_challenges_osint/Elementaire/elementaire_SH/">Elémentaire... Vous avez dit élémentaire ?</a></td>
                <td>6</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>OSINT</td>
                <td><a href="../ressources_challenges_osint/Espionnage/espionnage/">Espionnage</a></td>
                <td>2</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="../ressources_challenges_osint/OperationOverlord/operation_overlord/">Opération Overlord</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>OSINT</td>
                <td><a href="../ressources_challenges_osint/GitSecrets/gitsecrets/">Secrets de Forge</a></td>
                <td>3</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="../ressources_challenges_osint/TresorDisparu/tresor_disparu/">Trésor disparu</a></td>
                <td>6</td>
				<td>⭐⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="../ressources_challenges_osint/ADN/adn/">Un brin peut en cacher un autre</a></td>
                <td>4</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>OSINT</td>
                <td><a href="../ressources_challenges_osint/VeniVidiVicix6/venividivicix6/">(veni, vidi, vici)x6</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Programmation Python/Scratch</td>
                <td><a href="../ressources_challenges_prog/AgeGlace/age_glace">Age glaciaire</a></td>
                <td>3</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Programmation Python/Scratch</td>
                <td><a href="../ressources_challenges_prog/ArtExpo/art_expo">Art Expo</a></td>
                <td>12</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Programmation Python/Scratch</td>
                <td><a href="../ressources_challenges_prog/BlackbeardsTreasureMap/BlackbeardsTreasureMap">Blackbeard's Treasure Map</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Réseaux</td>
                <td><a href="../ressources_challenges_reseaux/BasLesMasques/bas_les_masques">Bas les masques !</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Réseaux</td>
                <td><a href="../ressources_challenges_reseaux/ReseauLocal/reseau_local">Réseau local</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Réseaux</td>
                <td><a href="https://www.challenges-kids.fr/categories/network/chall3/">TELNET</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Réseaux</td>
                <td><a href="../ressources_challenges_reseaux/TrameEthernet/trame_ethernet">Trame Ethernet</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="../ressources_challenges_stegano/AsciiArt/ascii_art">ASCII Art</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://www.challenges-kids.fr/categories/stegano/chall5/">Casse-tête chinois</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="../ressources_challenges_stegano/ImageMaBelleImage/image_ma_belle_image">Image, ma belle image</a></td>
                <td>3</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="../ressources_challenges_stegano/ImageCorrompue/image_corrompue">Image corrompue</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://www.challenges-kids.fr/categories/culture/chall2/">QR Code cassé</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://www.challenges-kids.fr/categories/culture/chall5/">QR Code gagnant</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="../ressources_challenges_web_client/CaptureTheFraise_N1/capture_the_fraise_N1">Capture The Fraise I</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="../ressources_challenges_web_client/CaptureTheFraise_N2/capture_the_fraise_N2">Capture The Fraise II</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="../ressources_challenges_web_client/CaptureTheFraise_N3/capture_the_fraise_N3">Capture The Fraise III</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="../ressources_challenges_web_client/CaptureTheFraise_N4/capture_the_fraise_N4">Capture The Fraise IV</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="../ressources_challenges_web_client/ConnexionAdministrateur/connexion_administrateur">Connexion administrateur</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href='https://www.challenges-kids.fr/categories/culture/chall3/'>Cookies or not cookies</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href='https://www.root-me.org/fr/Challenges/Web-Client/HTML-boutons-desactives'>HTML - boutons désactivés</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.challenges-kids.fr/categories/web/chall3/'>Répertoires</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.root-me.org/fr/Challenges/Web-Serveur/SQL-injection-Authentification'>SQL injection - authentification</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.root-me.org/fr/Challenges/Web-Serveur/SQL-injection-String'>SQL injection - String</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.challenges-kids.fr/categories/web/chall4/'>URL ?</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.challenges-kids.fr/categories/web/chall8/'>Tetris</a></td>
                <td>1</td>
				<td>⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐</td>
            </tr>
        </tbody>
    </table>
</center>

<script src='../tri_challenges.js'></script>