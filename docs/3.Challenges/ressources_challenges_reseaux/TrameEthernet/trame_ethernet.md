---
author: à compléter
title: Trame Ethernet
---

# Challenge : trame Ethernet

![](../trame_ethernet.png){: .center }

Vous faites partie d'une équipe de cybersécurité chargée de surveiller les agissements en ligne d'une organisation 
criminelle et vous venez de capter une trame Ethernet de type II émanant de l'ordinateur d'adresse IP `192.168.100.10` de 
cette dernière :

```
aa aa aa aa aa aa aa ab e1 52 47 6a 4b cd 45 02 0a bf 5f 41
08 00 45 00 00 34 96 69 40 00 40 06 5a 9b c0 a8 64 0a c3 11
d7 3b b5 4e 23 2a 0a d0 6c ad 7a 9a 30 c1 80 10 0e e5 89 91
00 00 01 01 08 0a 00 d6 4b 69 01 32 4b c4 17 ea 52 5f 86 3f
```

!!! note "Votre objectif"
    Déterminer les adresses MAC (au format hexadécimal) et IP (au format décimal pointé) de l'ordinateur à destination duquel ce message a été envoyé.

    Le flag est la concaténation de ces deux adresses, sans caractères spéciaux.

    Par exemple, si l'adresse MAC est `e5:46:8a:5b:21:a3` et l'adresse IP est `159.172.18.3`, alors le flag est 
    `e5468a5b21a3159172183`

??? abstract "Indice"
    <a href='../Trame_Ethernet_II.pdf' target='_blank'>Documentation sur la trame Ethernet II</a>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<br>
<p>
    <form id="form_trame_ethernet1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>Les trames Ethernet de type II n'ont aucun secret pour vous !</p>
</div>

<script src='../script_chall_trame_ethernet.js'></script>