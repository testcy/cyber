const flags = {'bas_les_masques1':['155.18.10.254',1,1],  // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
				'bas_les_masques2':['137.117.25.254',2,1],
				'bas_les_masques3':['145.110.255.254',3,1],
				'bas_les_masques4':['166.102.255.254',4,1],
				'bas_les_masques5':['166.102.223.254',5,0]};

 

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_bas_les_masques1.addEventListener( "submit", check_and_update);
form_bas_les_masques2.addEventListener( "submit", check_and_update);
form_bas_les_masques3.addEventListener( "submit", check_and_update);
form_bas_les_masques4.addEventListener( "submit", check_and_update);
form_bas_les_masques5.addEventListener( "submit", check_and_update);