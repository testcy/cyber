---
author: à compléter
title: Réseaux
---

# Réseaux

![](../ressources_challenges_reseaux/cyber.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Description générale de la catégorie"

    Un réseau informatique est une structure physique constituée de périphériques terminaux (ordinateurs, serveurs, etc.), 
    de périphériques intermédiaires (switchs, routeurs, etc.) et de connexions (filaire, sans fil, etc.) dans lequel transite 
    diverses données.

    Le principe même d'une attaque informatique est d'utiliser les failles des réseaux informatiques.


    Dans cette catégorie, on propose divers challenges ayant pour but de manipuler les notions de base des réseaux 
    informatiques tels que les adresses IP, les adresses MAC, les trames et les protocoles.

    



<hr style="height:5px;color:red;background-color:red;">

!!! note "Bas les masques !"

    ![](../ressources_challenges_reseaux/BasLesMasques/masque_venitien.png){: .center }
    
    Une adresse réseau et un masque de sous-réseau... Quelle adresse IP peut bien avoir cette machine ?

    Un challenge pour manipuler quelques calculs élémentaires sur les adresses !

    **Difficulté :** ⭐⭐ 5 flags

    [_Accéder au challenge_](../ressources_challenges_reseaux/BasLesMasques/bas_les_masques){ .md-button target="_blank" rel="noopener" }


!!! note "Réseau local"

    ![](../ressources_challenges_reseaux/ReseauLocal/reseau_local.png){: .center }
    
    Une attaque sur une infrastructure sensible dont vous êtes le chef de la sécurité informatique vient d'être détectée ! 
    Usez de toutes vos connaissances pour déterminer son origine et la stopper !

    Un challenge pour jouer avec les adresses IP.

    **Difficulté :** ⭐⭐ 1 flag

    [_Accéder au challenge_](../ressources_challenges_reseaux/ReseauLocal/reseau_local){ .md-button target="_blank" rel="noopener" }


!!! note "TELNET (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    ![](../ressources_challenges_reseaux/Challenges_externes/telnet.png){: .center }
    
    Un échange de trame TELNET vient d'être intercepté.

    Saurez-vous retrouver le mot de passe de l'utilisateur ?

    **Difficulté :** ⭐ 1 flag

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/network/chall3/){ .md-button target="_blank" rel="noopener" }


!!! note "Trame Ethernet"

    ![](../ressources_challenges_reseaux/TrameEthernet/trame_ethernet.png){: .center }
    
    Vous venez d'intercepter une trame Ethernet émanant d'un ordinateur d'une organisation criminelle que vous surveillez. 
    A qui ce message s'adresse-t-il ? Usez de toutes vos connaissances pour percer rapidement le mystère.

    **Difficulté :** ⭐ 1 flag

    [_Accéder au challenge_](../ressources_challenges_reseaux/TrameEthernet/trame_ethernet){ .md-button target="_blank" rel="noopener" }