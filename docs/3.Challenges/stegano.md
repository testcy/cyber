---
author: à compléter
title: Stéganographie
---

# Stéganographie

![](../ressources_challenges_stegano/ping2.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Description générale de la catégorie"

    La stéganographie est l'art de masquer une information dans une autre, comme, par exemple, un texte dans un texte, un texte dans une image ou encore une image dans une image.

    Dans cette catégorie, on propose divers challenges permettant de se familiariser avec quelques techniques simples de stéganographie.

    



<hr style="height:5px;color:red;background-color:red;">

!!! note "ASCII Art"

    ![](../ressources_challenges_stegano/AsciiArt/ascii-text-art.png){: .center }
    
    Votre meilleur agent vient de vous envoyer une information cruciale. Utilisez tout votre savoir-faire pour la décoder 
    au plus vite !

    **Difficulté :** ⭐⭐ 1 flag

    [_Accéder au challenge_](../ressources_challenges_stegano/AsciiArt/ascii_art){ .md-button target="_blank" rel="noopener" }

!!! note "Spectrale"

    Un groupe de hackers astucieux orchestrent des attaques informatiques complexes et apparemment indétectables. 


    **Difficulté :** ⭐⭐ 2 flags

    [_Accéder au challenge_](../ressources_challenges_stegano/Spectrale/spectrale){ .md-button target="_blank" rel="noopener" }


!!! note "Casse-tête chinois (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    ![](../ressources_challenges_stegano/Challenges_externes/casse_tete.png){: .center }
    
    Cela ressemble à une langue mais est-ce vraiment le cas ?

    Saurez-vous retrouver le message qui y est dissimulé ?

    **Difficulté :** ⭐ 1 flag

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/stegano/chall5/){ .md-button target="_blank" rel="noopener" }


!!! note "Image, ma belle image"

    ![](../ressources_challenges_stegano/ImageMaBelleImage/image_livre.png){: .center }
    
    Une image peut cacher bien des secrets. Découvrez quelques techniques de stéganographie et faites preuve de toute votre sagacité pour découvrir les messages cachés.

    **Difficulté :** ⭐⭐ 3 flags

    [_Accéder au challenge_](../ressources_challenges_stegano/ImageMaBelleImage/image_ma_belle_image){ .md-button target="_blank" rel="noopener" }


!!! note "Image corrompue"

    ![](../ressources_challenges_stegano/ImageCorrompue/image_corrompue.png){: .center }
    
    Un message caché dans une image qui n'en est peut-être pas une... ou pas... Quelques manipulations d'entête devraient 
    faire l'affaire...

    **Difficulté :** ⭐⭐ 1 flag

    [_Accéder au challenge_](../ressources_challenges_stegano/ImageCorrompue/image_corrompue){ .md-button target="_blank" rel="noopener" }


!!! note "QR Code cassé (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    ![](../ressources_challenges_stegano/Challenges_externes/qr_code_casse.png){: .center }
    
    Un QR Code qui a une drôle d'allure...

    Saurez-vous retrouver le message qui y est dissimulé ?

    **Difficulté :** ⭐ 1 flag

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/culture/chall2/){ .md-button target="_blank" rel="noopener" }


!!! note "QR Code gagnant (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    ![](../ressources_challenges_stegano/Challenges_externes/qr_code_gagnant.png){: .center }
    
    Scannez moi vite pour obtenir le flag... ou pas...

    Flasher, ou ne pas flasher, telle est la question ...

    **Difficulté :** ⭐ 1 flag

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/culture/chall5/){ .md-button target="_blank" rel="noopener" }