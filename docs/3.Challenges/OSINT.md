---
author: à compléter
title: OSINT
---

# OSINT

![](../ressources_challenges_osint/localisation2.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Description générale de la catégorie"

    L'OSINT (en anglais : _open source intelligence_ ; en français : _renseignement d'origine sources ouvertes_) est un 
    ensemble hétéroclite de pratiques d’investigation et d’analyse qui vise à dévoiler une information préalablement 
    dissimulée en récoltant, croisant ou analysant des données numériques disponibles en source ouverte (médias, web, données gouvernementales, publications professionnelles et académiques, données commerciales, etc.)

    L'OSINT est principalement utilisé dans le cadre d'activités liées à la sécurité nationale, l'application de la loi et l'intelligence économique dans le secteur privé. Il est particulièrement utilisé dans la lutte contre les _fake news_.

    Dans cette catégorie, on propose divers challenges d'entraînement à ces différentes pratiques.


<hr style="height:5px;color:red;background-color:red;">

!!! note "Elémentaire... Vous avez dit élémentaire ?"

    ![](../ressources_challenges_osint/Elementaire/image_SH_nuit.png){: .center }
    
    Suivez les pas d'un des plus célèbres détectives de l'Histoire.
    
    Une enquête de haut vol où vous devrez faire preuve de toute votre sagacité pour résoudre les différentes énigmes proposées.

    **Difficulté :** ⭐⭐ 6 flags

    [_Accéder au challenge_](../ressources_challenges_osint/Elementaire/elementaire_SH){ .md-button target="_blank" rel="noopener" }

!!! note "Espionnage (d'après Passe ton hack d'abord)"

    ![](../ressources_challenges_osint/Espionnage/image_SH.png){: .center }
    
    En tant qu'agent secret, vous devez exécuter une mission secrète à haut risque. Votre contact vous a donné rendez-vous dans un lieu secret... L'affaire ne fait que commencer...

    **Difficulté :** ⭐⭐ 2 flags

    [_Accéder au challenge_](../ressources_challenges_osint/Espionnage/espionnage){ .md-button target="_blank" rel="noopener" }

!!! note "Opération Overlord"

    ![](../ressources_challenges_osint/OperationOverlord/image_operation_overlord.png){: .center }
    <center><span style='color:gray;font-size:8pt;'>Photo de Robert F. Sargent (domaine public)</span></center>
    
    Plongez dans la plus grande bataille de la Seconde Guerre Mondiale et faites preuve de toute votre perspicacité pour résoudre les différentes énigmes qui vont sont proposées...

    **Difficulté :** ⭐⭐ 5 flags

    [_Accéder au challenge_](../ressources_challenges_osint/OperationOverlord/operation_overlord){ .md-button target="_blank" rel="noopener" }

!!! note "Secrets de Forge"

    ![](../ressources_challenges_osint/GitSecrets/git.png){: .center }

    Une équipe de cybersécurité qui n'a pas respecté les règles et a été supprimée. Retrouverez-vous leurs traces ?

    **Difficulté :** ⭐⭐ 3 flags

    [_Accéder au challenge_](../ressources_challenges_osint/GitSecrets/gitsecrets){ .md-button target="_blank" rel="noopener" }


!!! note "Trésor disparu"

    ![](../ressources_challenges_osint/TresorDisparu/tresor_perdu.png){: .center }

    Un trésor disparu depuis des siècles est sur le point de resurgir...
    
    Jeune étudiant en histoire et féru d'architecture, vous êtes sur le point de résoudre l'une des plus grandes énigmes de notre temps...

    **Difficulté :** ⭐⭐⭐ 6 flags

    [_Accéder au challenge_](../ressources_challenges_osint/TresorDisparu/tresor_disparu){ .md-button target="_blank" rel="noopener" }


!!! note "Un brin peut en cacher un autre"

    ![](../ressources_challenges_osint/ADN/adn.png){: .center }

    Plongez au coeur d'un génome mystérieux et explorez-le dans ses moindres recoins...

    Un brin d'ADN peut cacher bien des secrets...

    **Difficulté :** ⭐⭐⭐ 4 flags

    [_Accéder au challenge_](../ressources_challenges_osint/ADN/adn){ .md-button target="_blank" rel="noopener" }


!!! note "(veni, vidi, vici)x6"

    ![](../ressources_challenges_osint/VeniVidiVicix6/image_cesar.png){: .center }
    
    La Guerre des Gaules fait rage... César et Vercingétorix ont à nouveau levé des armées pour se combattre. Qui l'emportera ?

    **Difficulté :** ⭐⭐ 1 flag

    [_Accéder au challenge_](../ressources_challenges_osint/VeniVidiVicix6/venividivicix6){ .md-button target="_blank" rel="noopener" }
	