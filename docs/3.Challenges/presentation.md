---
author: à compléter
title: Introduction
---

!!! abstract "Présentation générale"

    <center><img src='../images_challenges/challenges_intro.png' style='width:50%;height:auto;'></center>

    Dans cette section, on propose différents challenges de niveau collège, Seconde, Première NSI et Terminale NSI.

    Ces challenges sont regroupés en différentes catégories :
    
    <center>
    <table style='border:0px none white;'>
    <tr style='background-color:white;border:0px none white;'>
    <td style='background-color:white;border:0px none white;'><center><img src='../images_challenges/challenges_algo.png'><br>
    <a href='../algorithmique'><strong>Algorithmique</strong></a></center></td>
    <td style='background-color:white;border:0px none white;'><center><img src='../images_challenges/challenges_crypto.png'><br>
    <a href='../crypto'><strong>Cryptographie</strong></a></center></td>
    </tr>
    <tr style='background-color:white;border:0px none white;'>
    <td style='background-color:white;border:0px none white;'><center><img src='../images_challenges/challenges_osint.png'><br>
    <a href='../OSINT'><strong>OSINT</strong></a></center></td>
    <td style='background-color:white;border:0px none white;'><center><img src='../images_challenges/challenges_prog.png'><br>
    <a href='../python'><strong>Programmation Python/Scratch</strong></a></center></td>
    </tr>
    <tr style='background-color:white;border:0px none white;'>
    <!-- site = acver.fr/dep_yve
    mdp = num_dep  + MODELE + num_dep -->
    <td style='background-color:white;border:0px none white;'><center><img src='../images_challenges/challenges_reseaux.png'><br>
    <a href='../reseaux'><strong>Réseaux</strong></a></center></td>
    <td style='background-color:white;border:0px none white;'><center><img src='../images_challenges/challenges_stegano.png'><br>
    <a href='../stegano'><strong>Stéganographie</strong></a></center></td>
    </tr>
    <tr style='background-color:white;border:0px none white;'>
    <td style='background-color:white;border:0px none white;'><center><img src='../images_challenges/challenges_web_client.png'><br>
    <a href='../web_client'><strong>Web - client</strong></a></center></td>
    <td style='background-color:white;border:0px none white;'><center><img src='../images_challenges/challenges_web_serveur.png'><br>
    <a href='../web_serveur'><strong>Web - serveur</strong></a></center></td>
    </tr>
    </table>
    </center>


    La liste complète des challenges proposés dans cette section est disponible <a href='../liste_challenges' target='_blank'>ici</a>. Pour plus de détails, rendez-vous sur la page dédiée de chaque catégorie.