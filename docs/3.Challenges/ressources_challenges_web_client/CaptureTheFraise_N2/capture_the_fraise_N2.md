---
author: à compléter
title: Capture The Fraise II
---

# Challenge : Capture The Fraise II

![](../fraise_N2.png){: .center }

!!! note "Votre objectif"
    Gagnez sur ce <a href='https://cybersecurite.forge.apps.education.fr/fraise/f/' target='_blank'>site</a> au moins 
    1 000 000 de fraises en moins de 15 secondes pour obtenir le flag.

    
??? abstract "Indice"
    Regardez les `VARIABLES` javascript.


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_capture_the_fraise_n2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>



<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>La console javascript n'a pas de secret pour vous !</p>
</div>

<script src='../script_chall_capture_the_fraise_N2.js'></script>