---
author: à compléter
title: Capture The Fraise I
---

# Challenge : Capture The Fraise I

![](../fraise_N1.png){: .center }

!!! note "Votre objectif"
    Ce <a href='https://cybersecurite.forge.apps.education.fr/fraise/f/' target='_blank'>site</a> contient un flag en trois parties. Vous devez reconstituer ce flag par concaténation.

    Chacune des parties est écrite au format `N1F1{flag1}`, `N1F2{flag2}` et `N1F3{flag3}`

    Par exemple, si on a trouvé `N1F1{carotte}`, `N1F2{banane}` et `N1F3{chocolat}`, alors le flag est `carottebananechocolat`

??? abstract "Indice"
    Quels sont les trois langages du Web ?


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_capture_the_fraise_n1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>



<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>Les trois langages du Web n'ont pas de secret pour vous !</p>
</div>

<script src='../script_chall_capture_the_fraise_N1.js'></script>