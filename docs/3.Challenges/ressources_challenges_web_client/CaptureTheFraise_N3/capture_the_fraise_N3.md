---
author: à compléter
title: Capture The Fraise III
---

# Challenge : Capture The Fraise III

![](../fraise_N3.png){: .center }

!!! note "Votre objectif"
    Maintenant que vous avez gagné 1 000 000 de fraises sur ce <a href='https://cybersecurite.forge.apps.education.fr/fraise/f/' target='_blank'>site</a>, vous ne voulez pas recommencer au début ! Pensez à exporter votre travail de façon lisible !

    
??? abstract "Indice"
    Connaissez-vous l'algorithme de Boyer-Moore ?


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_capture_the_fraise_n3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>



<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez réussi à exporter votre fichier : vous n'êtes pas aux fraises !</p>
</div>

<script src='../script_chall_capture_the_fraise_N3.js'></script>