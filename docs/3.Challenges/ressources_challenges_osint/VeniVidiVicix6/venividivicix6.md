---
author: à compléter
title: (veni, vidi, vici)x6
---

# Challenge : (veni, vidi, vici)x6

![](../image_cesar.png){: .center }

!!! note "Votre objectif"
    St Edéaia qyl ta déqeabe dp Zwzciykélwrii hwdane Gékir, uyi sctrp kjindp fsbaiwpw nut pr jmvaygzm unp égditayxw dicesaze.

    Lp jdig edx dm SHL-1 hm vom oi umttp fsbaiwpw xrécéoéi vc nox hm lépacxwuene eubuew sù wtle d'ikb sieyéw, boue gwki éccml mn mtrmacuwik, aand eukenew wb sayw uiranxèjms saégaiux ssja le nejictèci _ ici spvl à aépacij tes xsla.

    Pac ipmmpwi, vins wi uis dp ps jatlmdte d'Lpékqa, ll zatle o'Edéaia di kqtup hsvs lp héhirtpqwvt anxmml dp ps Kôte-o'Sj. Wn dpzjiit osfk fatvw te SSE-1 vm coei_v_wr_awikqa, szml 23mcc6107ne1t1j34701d9f9le4354sj498eco3i, um qut wwzaie pw nlar.


??? abstract "Indice"
    On se retrouve au métro le plus proche.
    <br><br>
    <center><img src = '../image_indice.png' style='width:50%;height:auto;'></center>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">


<p>
    <form id="form_VeniVidiVicix6">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>



<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>L'Histoire n'a pas de secret pour vous !</p>
</div>

<script src='../script_chall_VeniVidiVicix6.js'></script>