---
author: à compléter
title: Espionnage
---

# Challenge : espionnage

![](../image_SH.png){: .center }


<h2>Partie 2 : l'enlèvement</h2>
<p>Lorsque vous arrivez sur le lieu du rendez-vous, vous découvrez que l'agent a été enlevé avant que vous ne puissiez le rencontrer. Il est cependant parvenu à cacher son téléphone et à prendre deux photos avant que son téléphone ne soit confisqué.
<br><br>
Les photos, bien que prises en toute hâte, pourraient bien contenir des indices cruciaux pour vous mener au lieu où il est retenu en otage, celle de la seconde photo.</p>

!!! note "Votre objectif"
    Utilisez ces deux photos afin de localiser et libérer notre agent. Soyez attentif aux détails et prêt à faire preuve de déduction. Le sort de l'agent secret repose entre vos mains.

    Le temps presse, alors faites preuve de rapidité et de perspicacité.

    Le flag est le nom de la ville et le nom de la rue où il est retenu en otage, écrits dans leur langue d'origine, en minuscules, sans accents, sans caractères spéciaux et sans espaces.
    
    Par exemple, s'il s'agit de l'avenue des Champs-Elysées à Paris, on écrira `paris_avenue_champs_elysees`.

??? abstract "Indice 1"

    <center><img src = '../indice1.jpg' style='width:55%;height:auto;'></center>

??? abstract "Indice 2"

    <center><img src = '../indice2.jpg' style='width:55%;height:auto;'></center>


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">


<p>
    <form id="form_EspionnagePartie2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>



<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez accompli votre mission et nous connaissons à présent les avancées de nos ennemis.</p>
</div>

<script src='../script_chall_espionnage2.js'></script>