---
author: à compléter
title: Cryptographie
---

# Cryptographie

![](../ressources_challenges_crypto/sha1.png){: .center }


!!! tip "Sara Sellos"

    ![](../ressources_challenges_crypto/sara2.png){: .center }

    <a href="https://fr.linkedin.com/in/sara-sellos-16638480" target='_blank'>Sara Sellos</a> est chef de département à la <a href='https://www.defense.gouv.fr/dga' target='_blank'>DGA (Direction 
    Générale de l'Armement)</a> et est ex-coordinatrice sectorielle défense à l'<a href='https://cyber.gouv.fr/' target='_blank'>ANSSI (Agence Nationale de la Sécurité des Systèmes d'Information)</a>

    Elle développe bénévolement, avec Saelyx, le site <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids</a>. 
    


!!! abstract "Description générale de la catégorie"

    La cryptographie est l'art de protéger une information en la rendant illisible à l'aide de différents procédés, comme, par exemple, des permutations de lettres ou de groupes de lettres, ou encore en utilisant des calculs mathématiques plus ou moins compliqués.

    Dans cette catégorie, on propose divers challenges ayant pour but de se familiariser avec diverses techniques, des plus simples, comme les chiffrements de César ou Vigenère, aux plus compliqués, comme la manipulation de fonctions de hachage, en passant par diverses méthodes classiques, comme les chiffrements en base 64 ou en base 32.

    



<hr style="height:5px;color:red;background-color:red;">

!!! note "Cryptage renforcé"

    ![](../ressources_challenges_crypto/CryptageRenforcé/image_cryptage.png){: .center }
    
    Votre mission, si vous l'acceptez : pénétrez à l'intérieur d'un dossier hautement crypté et décodez le message chiffré qui s'y trouve.

    **Difficulté :** ⭐⭐⭐ 1 flag

    [_Accéder au challenge_](../ressources_challenges_crypto/CryptageRenforcé/cryptage_renforce){ .md-button target="_blank" rel="noopener" }



!!! note "Dossier protégé"

    ![](../ressources_challenges_crypto/DossierProtégé/image_dossier.png){: .center }
    
    Notre agent a caché un message secret dans un dossier protégé par un mot de passe.

    Votre mission : pénétrez à l'intérieur de ce dossier et décodez le message avant qu'il ne soit trop tard...

    **Difficulté :** ⭐⭐ 1 flag

    [_Accéder au challenge_](../ressources_challenges_crypto/DossierProtégé/dossier_protege){ .md-button target="_blank" rel="noopener" }



!!! note "Grille tournante (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    ![](../ressources_challenges_crypto/Challenges_externes/Handbuch_der_Kryptographe.png){: .center }
    
    Dix ans après son célèbre tour du monde en 80 jours, Phileas Fogg se lance dans une nouvelle expédition à la 
    poursuite d'un trésor légendaire : celui qui est caché dans la célèbre caverne d'Ali Baba. Après avoir réussi à 
    modifier le mot de passe magique qui protège l'entrée de la caverne, l'explorateur le chiffre avant de le 
    transmettre à ses compagnons de fortune...

    Arriverez-vous à le déchiffrer ?

    **Difficulté :** ⭐⭐ 1 flag

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/crypto/chall7/){ .md-button target="_blank" rel="noopener" }
