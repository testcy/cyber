---
author: à compléter
title: ASCII Art
---

# Challenge : ASCII Art

![](../ascii-text-art.png){: .center }

En tant que responsable d'une agence de renseignements, vous venez de recevoir de la part de votre meilleur agent un 
message contenant l'adresse IP (au format IPv4) de l'ordinateur distant d'une organisation criminelle.

!!! note "Votre objectif"
    Déterminer l'adresse IP contenue dans ce <a href='../ascii-art.txt' download='ascii-art.txt'>fichier</a>.

    Le flag est cette adresse au format décimal pointé (par exemple `192.168.10.137`).


<figure>
  <figcaption>A écouter ou voir</figcaption>
  <audio controls src="../Coagula01.wav"></audio>
  <a href="../Coagula01.wav"> Téléchargement audio </a>
  <br>
</figure>

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<br>
<p>
    <form id="form_ascii_art1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>L'adresse IP a été décodée avec succès et vous pouvez maintenant suivre l'activité de l'organisation.</p>
</div>

<script src='../script_chall_ascii_art.js'></script>