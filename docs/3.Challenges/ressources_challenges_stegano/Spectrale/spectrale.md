---
author: à compléter
title: Spectrale
---

# Challenge : Spectrale


Carla est une experte en cybersécurité travaillant pour une agence gouvernementale française. Un lundi matin, elle reçoit une alerte : une activité suspecte a été détectée dans le réseau national. Après des heures de travail acharné, Carla découvre qu'un groupe de hackers a infiltré le système. Ce groupe vise à paralyser des infrastructures critiques du pays.

Carla sait qu'elle n'a pas beaucoup de temps pour contrer cette menace. Elle se plonge dans l'analyse des données, remontant la piste numérique laissée par les hackers et trouvent des documents.



!!! note "Votre objectif"
    Trouver le login et le mot de passe du serveur ennemi.

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Enigme 1 Login</h2>
<p>
Carla a trouvé ce <a href='../login.zip' download='login.zip'>fichier compréssé</a> protégé par un mot de passe. 
Un autre fichier contenant la phrase énigmatique : Bachir ADEL RAULESE a écrit le livre CHALEUR DE LA BRAISE l'interpelle.

<p><strong>Indice :</strong> le mot de passe du fichier compréssé est de la forme mot1-mot2</p>

</p>
<br>
<p>
    <form id="form_spectrale1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Partie2' hidden>
<br>
<h2>Enigme 2 Mot de passe</h2>
<br>
<figure>
  <figcaption>A écouter ou à voir !</figcaption>
  <audio controls src="../Coagula12.wav"></audio>
  <a href="../Coagula12.wav"> Téléchargement de l'audio </a>
  <br>
</figure>
<br>
<p><strong>Indice :</strong> un son n’est pas qu’un son...</p>
<br>

<p>
    <form id="form_spectrale2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>


<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>
Avec ces informations en main, Carla initie un contre-attaque immédiate. En collaboration avec les autorités, elle orchestre une série de contre-mesures pour renforcer la sécurité autour des infrastructures ciblées. 
En sortant de son bureau, Carla lève les yeux vers le soleil levant. Elle prend un instant pour savourer la tranquillité retrouvée, consciente que la confiance et la vigilance seront ses plus fidèles alliées dans ce monde numérique complexe et dangereux.

</p>
</div>

<script src='../script_chall_spectrale.js'></script>