---
author: à compléter
title: Image corrompue
---

# Challenge : image corrompue

![](../image_corrompue.png){: .center }

Cette <a href='../photo.png' download='photo.png'>image</a> png semble avoir un petit problème. Saurez-vous le contourner pour récupérer le flag situé dedans ?

!!! note "Votre objectif"
    Déterminer le flag caché dans l'image.

??? abstract "Indice"
    Savez-vous ce qu'est une signature png ?

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<br>
<p>
    <form id="form_image_corrompue1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez réussi à récupérer le flag en manipulant l'entête du fichier png.</p>
</div>

<script src='../script_chall_image_corrompue.js'></script>