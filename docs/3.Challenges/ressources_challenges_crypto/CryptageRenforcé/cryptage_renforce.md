---
author: à compléter
title: Cryptage renforcé
---

# Challenge : cryptage renforcé

![](../image_cryptage.png){: .center }

!!! note "Votre objectif"

    Obtenir le flag en clair caché dans un dossier hautement crypté.


??? abstract "Indice 1"
    <center><img src='../image_indice.jpg' style='width:30%;height:auto;'></center>


??? abstract "Indice 2"
    <a href='https://www.aperisolve.com/' target='_blank'>Une photo n'est pas qu'une image...</a>

??? abstract "Indice 3"

    64 32 45 : cux kw Osaq, qnl vr Tiee ! Bs xnmb êfrx bf Pzmr eg Jqowz
    
    DY94R8IQ8TK8ST9Z09-R9%Z8769ZT69U91O6J+9D+94V9SK9 U9\$1TIK9TE6RZ9\*09EF96WMDG9HVO+FIKNTO09OPS81IOT9 A9%1IQLSPZ9P09H1IJA9RVMTG9:M6CPTCZA-\*9APSH09DA9LH9-VIJO8\*K9\$A9-OBW1HJGSLO9VE6LFN\$E8Y6MBM981HXU80L9FFATN6LO9
    
    wubrxpfgw UP5 dx tvc : 621u2963nr87d182x3kw4p6336689288
    
    kiusby eqh+xpm

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_cryptage_renforce1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez trouvé le flag !</p>
</div>

<script src='../script_chall_cryptage_renforce.js'></script>