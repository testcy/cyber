---
author: à compléter
title: Dossier protégé
---

# Challenge : dossier protégé

![](../image_dossier.png){: .center }

Un document chiffré a été déposé par l'un de nos agents dans un dossier distant protégé auquel on accède en cliquant sur ce <a href='https://nuage03.apps.education.fr/index.php/s/TqXDC3m5JiR3AT3' target='_blank'>lien</a>.

Malheuseusement, il a été éliminé par nos adversaires avant d'avoir pu nous communiquer le mot de passe. Il a toutefois eu le temps de laisser derrière lui les trois indices suivants qui nous permettront d'accéder au dossier.


??? abstract "Indice 1"

    <center><img src='../image1.png' style='width:60%;height:auto;'></center>


??? abstract "Indice 2"
    
    <center><img src='../image2.png' style='width:60%;height:auto;'></center>

??? abstract "Indice 3"

    <a href='../../../presentation' target='_blank'>Remonter à la source entre 960 et 980 av. J.-C.</a>

!!! note "Votre objectif"
    Utiliser ces trois indices pour accéder au contenu du dossier, puis déchiffrer le message qui s'y trouve.

    Le flag est le message en clair.

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_dossier_protege1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez réussi à décoder le message !</p>
</div>

<script src='../script_chall_dossier_protege.js'></script>