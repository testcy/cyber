---
author: à compléter
title: Algorithmique
---

# Algorithmique

![](../ressources_challenges_algo/mdp9.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Description générale de la catégorie"

    La compréhension des algorithmes sous-jacents aux codes informatique qui ont été implémentés dans certaines applications est souvent un vecteur de nombreuses attaques.
    
    Dans cette catégorie, on propose divers challenges ayant pour but la compréhension à la main de codes informatiques.

!!! warning "Attention"

    Pour que ces challenges aient tout leur intérêt, il est recommandé de ne pas implémenter les algorithmes proposés et de ne pas les exécuter sur machine. Ceux-ci doivent être exécutés à la main.



<hr style="height:5px;color:red;background-color:red;">

!!! note "Blackbeard’s Hidden Treasures (d'après <a href='https://www.101computing.net/'>101computing.net</a>)"

    ![](../ressources_challenges_algo/BlackbeardsHiddenTreasures/pirate-flag.png){: .center }
    
    Le célèbre pirate Barbe-Noire a caché son trésor en douze lieux différents. Différents fragments de parchemins, qui devraient indiquer ces lieux, viennent tout juste d'être découverts dans une salle obscure d'une bibliothèque. Saurez-vous faire preuve de sagacité et de perspicacité pour les déchiffer et mettre enfin à jour ce fabuleux trésor ?

    **Difficulté :** ⭐⭐ 12 flags

    !!! warning "Challenge en anglais"

    [_Accéder au challenge_](../ressources_challenges_algo/BlackbeardsHiddenTreasures/BlackbeardsHiddenTreasures){ .md-button target="_blank" rel="noopener" }


!!! note "Cadenas"

    ![](../ressources_challenges_algo/Cadenas/coffre_cadenas.png){: .center }
    
    Après des années de recherche, vous venez enfin de localiser un coffre contenant un incroyable trésor. Arrivé sur 
    place, vous vous apercevez que son système d'ouverture est incroyablement complexe et qu'il vous faudra user de toute 
    votre perspicacité pour résoudre les cinq puzzles de codage nécessaires...

    **Difficulté :** ⭐⭐ 5 flags

    [_Accéder au challenge_](../ressources_challenges_algo/Cadenas/coffre_cadenas){ .md-button target="_blank" rel="noopener" }
	
!!! note "Cadenas dynamiques"

	![](../ressources_challenges_algo/CadenasAlea/cadenas.png){: .center }

    La suite du challenge 
    <a href="https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/ressources_challenges_osint/ADN/adn/" target="_blank">Un brin peut en cacher un autre</a>. Il n'est pas nécessaire de l'avoir terminé. Mais c'est la première apparition du petit Marius, l'assistant du Dr Kalifir.

    Dans ce nouveau challenge il va montrer l'étendue de ses compétences pour ouvrir ces cadenas, suscitant l'admiration du Dr Kalifir et du Dr Kalifounet.
    

    **Difficulté :** ⭐ 3 flags

    [_Accéder au challenge_](../ressources_challenges_algo/CadenasAlea/cadenas){ .md-button target="_blank" rel="noopener"}
