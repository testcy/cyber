---
author: à compléter
title: Introduction
---

!!! abstract "Présentation générale"

    <center><img src='../plateformes_challenges.png' style='width:50%;height:auto;'></center>

    Cette section présente diverses plateformes de challenges très intéressantes pour s'initier et se former à la cybersécurité, ainsi que pour approfondir ses connaissances en informatique.

    Pour chacune des plateformes présentées, on donne une description générale de son contenu, le type et le niveau des challenges proposés, ainsi que le lien vers la plateforme.

    Pour plus de lisibilité, elles sont regroupées en trois catégories :

    <center>
    <table style='border:0px none white;'>
    <tr style='background-color:white;border:0px none white;'>
    <td style='text-align:center; background-color:white;border:0px none white;'><strong>OSINT</strong></td>
    <td style='text-align:center; background-color:white;border:0px none white;'><strong>Cryptographie</strong></td>
    <td style='text-align:center; background-color:white;border:0px none white;'><strong>Cybersécurité</strong></td>
    </tr>
    <tr style='background-color:white;border:0px none white;'>
    <td style='border:0px none white;'><center><img src='../Image_OSINT.png' style='width:100%;height:auto;'></center></td>
    <td style='border:0px none white;'><center><img src='../Image_crypto.png' style='width:94%;height:auto;'></center></td>
    <td style='border:0px none white;'><center><img src='../Image_cyber.png' style='width:94%;height:auto;'></center></td>
    </tr>
    <tr style='background-color:white;border:0px none white;'>
    <td style='text-align:center;background-color:white;border:0px none white;'><a href='../OZINT'>OZINT</a>
    <br>
    <a href='../TOP'>TOP - The Osint Project</a></td>
    <td style='text-align:center;background-color:white;border:0px none white;'><a href='../Cryptohack'>Cryptohack</a></td>
    <td style='text-align:center;background-color:white;border:0px none white;'><a href='../Challenges-kids'>Challenges-kids</a>
    <br>
    <a href='../Try_Hack_Me'>Try Hack Me</a>
    <br>
    <a href='../PicoCTF'>PicoCTF</a>
    <br>
    <a href='../Root_Me'>Root Me</a>
    <br>
    <a href='../Hackropole'>Hackropole</a></td>
    </tr>
    </table>
    </center>