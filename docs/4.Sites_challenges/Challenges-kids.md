---
author: à compléter
title: Challenges-kids
---

# Challenges-kids

![](../images_sites_challenges/Plateforme_challenges-kids.png){: .center }


!!! abstract "Présentation générale de la plateforme"

    **Challenges-kids** est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands. On y trouve de nombreux challenges de difficultés graduées pour progresser et répartis dans différentes catégories. Après chaque résolution de challenge, un point pédagogique est réalisé pour illustrer ou commenter la notion abordée.


    - **Type de challenges :** généraliste
    - **Niveau des challenges :** débutant
    - **Lien vers la plateforme :** <a href='https://www.challenges-kids.fr/index.php' target='_blank'>https://www.challenges-kids.fr/index.php</a>