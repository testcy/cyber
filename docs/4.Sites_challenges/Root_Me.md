---
author: à compléter
title: Root Me
---

# Root Me

![](../images_sites_challenges/Plateforme_root_me.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Présentation générale de la plateforme"

    **Root Me** est une plateforme de CTF française très connue et très qualitative qui offre de nombreux challenges dans
    diverses catégories. Chaque challenge aborde en général un type de faille bien particulier et nous permet d’apprendre en
    l’exploitant, notamment grâce aux ressources qui sont fournies et qui décrivent, soit la faille elle-même, soit des techniques possibles pour l'exploiter.


    - **Type de challenges :** généraliste
    - **Niveau des challenges :** bon niveau
    - **Lien vers la plateforme :** <a href='https://www.root-me.org/' target='_blank'>https://www.root-me.org/</a>