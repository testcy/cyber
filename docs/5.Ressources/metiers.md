---
author: à compléter
title: Métiers 
---

# Métiers des spécialistes de la cyber

<figure>
<center><img src='../metiersCyber1.jpg' alt='image'></center>
  <figcaption>
    Campagne demain spécialiste cyber - © ANSSI
  </figcaption>
</figure>


La filière de cybersécurité propose une diversité d'emplois souvent méconnue : informatique, droit, relations internationales... tous les profils sont donc les bienvenus. Les femmes, en particulier, peuvent y trouver des emplois épanouissants et motivants dans un milieu considéré, à tort, comme exclusivement réservé aux hommes.

## Découvrir les métiers des Spécialistes Cyber

<a href='https://www.demainspecialistecyber.fr/les-metiers?metier=coordinatriceSecurite' target='_blank'>Juliette - Coordinatrice sécurité</a>

<a href='https://www.demainspecialistecyber.fr/les-metiers?metier=cheffePentest' target='_blank'>Julie - Cheffe d'équipe pentest</a>

<a href='https://www.demainspecialistecyber.fr/les-metiers?metier=formatriceCyber' target='_blank'>Maryline - Formatrice en cybersécurité</a>

<a href='https://www.demainspecialistecyber.fr/les-metiers?metier=chercheuseSI' target='_blank'>Caroline - Chercheuse en sécurité des SI</a>

<a href='https://www.demainspecialistecyber.fr/les-metiers?metier=evaluatriceSecurite' target='_blank'>Anne - Évaluatrice de la sécurité des technologies de l'information</a>



##  6 grandes catégories de métiers dans le domaine cyber

<a href='https://youtu.be/ZlPv_JaanxM' target='_blank'>Demain… cryptologue</a>

<a href='https://youtu.be/lC8hKgEdI1c' target='_blank'>Demain… responsable cybersécurité</a>

<a href='https://www.linkedin.com/pulse/%C3%AAtre-juriste-au-comcyber-commandement-de-la-cyberdefense/' target='_blank'>Demain… juriste cyber</a>

<a href='' target='_blank'>Demain… analyste des cybermenaces</a>

<a href='https://youtu.be/yRZ1QtLMENE' target='_blank'>Demain… expert et experte cyberdéfense</a>

<a href='ttps://www.linkedin.com/pulse/florian-r%C3%A9serviste-op%C3%A9rationnel-de-cyberd%C3%A9fense--1c/' target='_blank'>Demain… hacker et hackeuse éthique</a>


## C'est aussi d'autres métiers…

<a href='https://cyber.gouv.fr/sinformer-sur-les-metiers-de-la-cybersecurite' target='_blank'>S'informer sur les métiers de la cybersécurité</a>

<a href='https://cyber.gouv.fr/publications/panorama-des-metiers-de-la-cybersecurite' target='_blank'>Découvre plus de métiers de la cybersécurité dans le Panorama des métiers de la cybersécurité, un document proposé par l’ANSSI</a>




