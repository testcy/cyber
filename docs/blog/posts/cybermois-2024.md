---
title: Cybermoi/s 2024
date: 2024-08-06
categories:
  - Général
  - Action
---

# Cybermoi/s 2024 : un mois pour devenir #CyberEngagé

![](/blog/images/BADGE_CYBER.png){: .center }

Tout au long du mois d’octobre 2024, **des activités vont être organisées en France et en Europe autour des enjeux de cybersécurité** : 

<!-- more -->
Événement de lancement, événements de sensibilisation, campagnes vidéo... 
Comme chaque année, un panel d’acteurs publics, privés et associatifs se mobiliseront pour proposer un programme de sensibilisation pédagogique à destination de tous les publics et ainsi développer une culture européenne cyber commune.

- <a href="https://www.cybermalveillance.gouv.fr/tous-nos-contenus/actualites/2024-cybermois" target="_blank">Le cybermois en détail</a>


- <a href="https://www.cybermalveillance.gouv.fr/tous-nos-contenus/actualites/cybermois-2024-ressources-grand-public" target="_blank">Ressources pour créer votre événement grand public</a>

![](../images/aeif.png){: .center }

 L’association <a href="https://aeif.fr/" target="_blank">AEIF</a> (Association des Enseignantes et Enseignants d'Informatique de France) s’engage pour le Cybermoi/s avec l’organisation d'événements et le relais des opérations du Cybermoi/s. 

Et vous, êtes-vous **#CyberEngagés** ? 

Animez des ateliers dans vos collèges ou lycées.

 - sur <a href=" https://cybersecurite.forge.apps.education.fr/cyber/1.Bonnes_pratiques/presentation/" target="_blank">les bonnes pratiques ou hygiène numérique</a> qui englobe un ensemble de pratiques visant à protéger les données personnelles et à assurer la sécurité en ligne ou sur le <a href="https://cybersecurite.forge.apps.education.fr/cyber/1.Bonnes_pratiques/cyberharcelement/" target="_blank">cyberharcèlement</a>

- en mettant à l'épreuve vos compétences en cybersécurité avec<a href=" https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/presentation/" target="_blank">  des challenges </a>qui vous permettent de vous entraîner tout en vous amusant. La <a href="https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/liste_challenges/
 " target="_blank">liste des challenges </a>que vous pouvez trier selon la catégorie, le titre du challenge, le nombre de flags ou la difficulté. .

- avec des <a href=" https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/parcours/" target="_blank"> parcours adaptés</a> à votre niveau mélangeant plusieurs catégories comme l'OSINT, la cryptographie, l'algorithmique...


 

