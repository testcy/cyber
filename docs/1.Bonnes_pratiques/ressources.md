---
author: à compléter
title: Ressources pour approfondir
---

# Ressources pour approfondir

**IMAGE**


!!! tip "femme"

    à compléter


!!! abstract "Explications de la partie"

    à compléter

[Dix règles d'or préventives](https://cyber.gouv.fr/dix-regles-dor-preventives)
La réduction des risques induits par l’usage des technologies numériques repose d’abord sur le respect de bonnes pratiques à adopter.


Pour informer et sensibiliser les publics sur les menaces numériques, le dispositif Cybermalveillance.gouv.fr met à disposition divers contenus thématiques.
[Liste des ressources mises à disposition par le dispositif Cybermalveillance.gouv.fr](https://www.cybermalveillance.gouv.fr/tous-nos-contenus/actualites/liste-des-ressources-mises-a-disposition) comme par exemple :
- Le chantage à l’ordinateur ou à la webcam prétendus piratés 
- L’escroquerie aux faux ordres de virement (FOVI)
- La fuite ou violation de données personnelles
- L’hameçonnage (phishing en anglais)


Comprendre les menaces et adopter les bonnes pratiques !
- Découvrez les mécanismes des principales menaces sur Internet et apprenez à mieux vous en protéger.
A l'issue de l’e-sensibilisation une attestation de suivi vous sera remise.
Programme de l'[e-sensibilisation](https://www.cybermalveillance.gouv.fr/sens-cyber/apprendre) 



Dans le cadre de sa mission de sensibilisation, l’ANSSI propose [SecNumacadémie](https://cyber.gouv.fr/le-mooc-secnumacademie) pour former le plus grand nombre de citoyens à la sécurité du numérique.



